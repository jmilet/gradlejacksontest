package com.jmi;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.ByteArrayOutputStream;


public class Main {
    public static void main(String[] arg) throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();

        Car car = new Car("yellow", "renault");
        //objectMapper.writeValue(new File("target/car.json"), car);
        //objectMapper.writeValue(System.out, car);

        String json = objectMapper.writeValueAsString(car);
        System.out.println(json);

        Car newCar = objectMapper.readValue(json, Car.class);
        System.out.println(newCar);
    }
}
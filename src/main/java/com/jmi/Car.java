package com.jmi;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;


public class Car {

    private String color;
    private String type;

    private List<String> saludo = Arrays.asList("hola", "que", "tal");

    public Car() {}

    public Car(String color, String type) {
        this.color = color;
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public List<String> getSaludo() {
        return saludo;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append(String.format("color: %s", this.color));
        sb.append(String.format(", type: %s", this.type));
        sb.append(String.format(", saludo: [%s]", this.saludo.stream().collect(Collectors.joining(","))));

        return sb.toString();
    }
}
